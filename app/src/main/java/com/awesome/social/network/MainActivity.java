package com.awesome.social.network;

import androidx.appcompat.app.AppCompatActivity;

import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener {

    private WebView webView;
    private TextView title, text;
    private ImageView imageView;
    private NetworkStateReceiver networkStateReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        webView =(WebView)findViewById(R.id.webView);

        // Custom WebViewClient to handle event on WebView.
        webView.setWebViewClient(new WebViewClient());
        title = (TextView) findViewById(R.id.title);
        text = (TextView) findViewById(R.id.text);
        imageView = (ImageView) findViewById(R.id.imageView);

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onStart() {
        super.onStart();
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.loadUrl(BuildConfig.WEB_SITE_URL);

        if (!DetectConnection.checkInternetConnection(this)) {
            networkUnavailable();
        } else {
            networkAvailable();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
    }

    @Override
    public void networkAvailable() {
        Log.d("[TEST]", "networkAvailable");
        title.setVisibility(View.INVISIBLE);
        text.setVisibility(View.INVISIBLE);
        imageView.setVisibility(View.INVISIBLE);
        webView.setVisibility(View.VISIBLE);
    }

    @Override
    public void networkUnavailable() {
        Log.d("[TEST]", "networkUnavailable");
        title.setVisibility(View.VISIBLE);
        text.setVisibility(View.VISIBLE);
        imageView.setVisibility(View.VISIBLE);
        webView.setVisibility(View.INVISIBLE);
    }
}
